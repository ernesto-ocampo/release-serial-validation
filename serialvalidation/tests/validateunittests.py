"""
Unit tests for validate_serial function.

I used TDD to develop validate_serial, so this module contains
tests from the very start - including testing the existence
of the function.
"""

import datetime
import pytest

from serialvalidation.validateserial import validate_serial


def test_func_exists():
    assert callable(validate_serial)


def test_accepts_one_parameter():
    validate_serial('20170312')  # assert no exception


def test_returns_bool():
    assert isinstance(validate_serial('20170312'), bool)


def test_accepts_string_and_unicode():
    # assert no exception:
    validate_serial('20170312')
    validate_serial(u'20170312')


@pytest.mark.parametrize('value', [
    123,
    123.4,
    datetime.date.today()
])
def test_type_error_if_not_string(value):
    with pytest.raises(TypeError):
        validate_serial(value)


YEAR_SPAN = 6  # ensure a leap year is covered


def generate_positive_cases(use_dot):
    """ Generate positive examples.

    Generates all positive examples of serial numbers
    in an n-year windows around today. Window size given by
    YEAR_SPAN.

    :param use_dot: if True, generate YYYYMMMDD.# format, otherwise,
    generate YYYYMMDD format.
    """
    range_start = - int(365.0 * YEAR_SPAN / 2)
    range_end = - range_start
    for delta in range(range_start, range_end + 1):
        date_value = datetime.date.today() + datetime.timedelta(days=delta)
        str_value = "{:%Y%m%d}".format(date_value)
        if use_dot:
            str_value += ".{}".format(delta % 20 + 1)
        yield str_value


@pytest.mark.parametrize('use_dot', [False, True])
def test_positive_cases(use_dot):
    """Test positive examples.

    Tests all dates in an n-year window around today.
    Window size defined by YEAR_SPAN.
    :param use_dot: if True, test YYYYMMMDD.# format, otherwise,
    test YYYYMMDD format
    """
    for serial in generate_positive_cases(use_dot):
        assert validate_serial(serial)


@pytest.mark.parametrize('value', [
    '',
    'hello',
    '20170312extrachars',
    'extrachars20170312',
    'release-20170312',  # "release" is not part of the serial number
    '20170312.',
    '20170312.a',         # non-number after .
    '20170312.-1',        # non-number after .
    '20170312.1.2',       # non-number after .
    '2017312',            # wrong format, too short
    '201732.1',           # wrong format with same length as YYYYMMDD
    '2017/3/2',           # wrong format with same length as YYYYMMDD
])
def test_invalid_syntax(value):
    with pytest.raises(ValueError) as exc:
        validate_serial(value)
    assert "Invalid format" in str(exc)


def test_too_long():
    with pytest.raises(ValueError) as exc:
        validate_serial('20170312.12345678901234')  # max digits exceeded
    assert "Too long" in str(exc)


@pytest.mark.parametrize('value', [
    '20171301',  # Invalid month
    '20170332',  # Invalid day
    '00000312',  # Invalid year
    '20170229',  # 2017 is not a leap year
])
def test_invalid_date(value):
    with pytest.raises(ValueError) as exc:
        validate_serial(value)
    assert "Invalid date" in str(exc)

    with pytest.raises(ValueError) as exc:
        validate_serial(value + '.5')  # also test with a digit
    assert "Invalid date" in str(exc)


def test_invalid_number():
    # number cannot be 0
    with pytest.raises(ValueError) as exc:
        validate_serial('20170312.0')
    assert "Invalid number" in str(exc)
