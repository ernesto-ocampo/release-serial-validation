"""
Provides validation functionality for Ubuntu Server release serial numbers.

This module provides validation for Ubuntu Server release serial numbers.
You can see some examples here: http://cloud-images.ubuntu.com/releases/16.04/
Valid release serial numbers take the form of YYYYMMDD or YYYYMMDD.#

You can use this module as a CLI script, in this form::

    python validateserial.py 20160420.3

Exit codes:
    0: The script ran correctly and the provided serial is valid.
        "Ok" is printed to stdout
    3: The script ran correctly and the provided serial is not valid.
       An error message is printed to stderr.
   Other standard exit codes apply (e.g. wrong args = 2)

You can also reuse the validation function validate_serial from
other python code.
"""

import argparse
from datetime import date
import re
import sys

# I assume that '#' in the given format 'YYYYMMDD.#' represents
# "1 or more digits". For security, up to 10 digits are allowed.
# Looking at the examples, ".0" is not valid

SERIAL_REGEX = \
    r'^(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})(\.(?P<num>\d{1,10}))?$'

MAX_LENGTH = 19  # keep in sync with SERIAL_REGEX

# This regex could be more specific, like
# ^[12][0-9]{3}(0[1-9]|1[0-2])([012][0-9]|3[01])(\.[0-9]+)?$ .
# However I don't think this is worth the extra complexity, error-proneness
# and difficulty of reading, since we need to do a semantic
# for valid dates anyway.


def validate_serial(serial):
    if not isinstance(serial, basestring):
        raise TypeError("Not a string")

    if len(serial) > MAX_LENGTH:
        raise ValueError("Too long")

    # This is a conscientious use of regular expressions. I am aware that
    # overuse of regular expressions is typical of amateur programming:
    # regular expression matching is exponential-time in the worst case.
    # However, in this case there is no risk because the regex does not allow
    # backtracking to try another, and also the regex will never match a
    # string longer than 19 characters. This max length is also checked
    # manually for added security. Because inputs longer than 19 chars are
    # ignored, this validation has asymptotic time complexity O(1) - there's
    # an upper bound. The advantage of using a regular expression is easier
    # validation and extraction of data parts from the string.

    match = re.match(SERIAL_REGEX, serial)

    if not match:
        raise ValueError("Invalid format")

    # these are safe conversions, as the regex guarantees numeric format:
    year = int(match.group('year'))
    month = int(match.group('month'))
    day = int(match.group('day'))

    try:
        date(year, month, day)
    except ValueError as e:
        raise ValueError("Invalid date: " + str(e))

    num_group = match.group('num')
    if num_group is not None and int(num_group) <= 0:
        raise ValueError("Invalid number: must be positive")

    return True


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('serial', help='Release serial number to validate')

    args = parser.parse_args()
    serial = args.serial

    try:
        validate_serial(serial)
        sys.stdout.write('Ok\n')
        # success, exit code 0
    except ValueError as err:
        sys.stderr.write(str(err) + '\n')
        sys.exit(3)


if __name__ == '__main__':
    main()
