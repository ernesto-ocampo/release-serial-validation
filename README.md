Release serial number validation for Ubuntu Server
===============================

[![build status](https://gitlab.com/ernesto-ocampo/release-serial-validation/badges/master/build.svg)](https://gitlab.com/ernesto-ocampo/release-serial-validation/pipelines)


## Validation

This package provides validadtion functionality for Ubuntu Server
release numbers. Valid release serial numbers take the form of YYYYMMDD or 
YYYYMMDD.#
Examples here: http://cloud-images.ubuntu.com/releases/16.04/

### Python validation framework

The `validateserial.py` module provides the `validate_serial` function
which you can use as::

    >>> validate_serial('20170312.5')
    True

### Python validation script

You can run `validateserial.py` module passing in the serial as argument::

    python validateserial.py 201703.5
    Ok

Exit codes:
 - 0: The script ran correctly and the provided serial is valid. "Ok" is printed to stdout
 - 3: The script ran correctly and the provided serial is not valid. An error message is printed to stderr.
 - Other standard exit codes apply (e.g. wrong args = 2)

### Bash validation script

The bash script `validate-serial.sh` provides validation functionality
as a bash script. Usage example::

    validate-serial.sh 20160420.3

Exit codes:
 - 0: The script ran correctly and the provided serial is valid. "Ok" is printed to stdout
 - 2: Wrong args
 - 3: The script ran correctly and the provided serial is not valid. An error message is printed to stderr.

# Design notes

The python validation script has been implemented as a python package
because this provides the followig advantages:

- One can easily install the script using `pip install serialvalidation` (once deployed to a pypi repo) - no need to manually download the script.
- It is possible to easily reuse the `validate_serial` function from other python code
- One can still run the script easily, even when installing using `pip`, by running `python -m serialvalidation 20170312`

There is a disadvantage to this approach: added code complexity. If the
advantages mentioned are not relevant, we can simply keep only the
`validateserial.py` file.

# Development approach

Development was carried out using TDD approach, creating unit tests first,
therefore there are plenty of unit tests available. System-wide tests were
written as well, testing the entire script execution and exit codes.

# Continuous Integration

CI was set up in GitLab, running all tests (no deployment).



